/*
Tadeusz Puźniakowski 2022
*/

const drawGcodeOnCanvas = (width, height, canvas, gcode, bit_radius) => {
    const scale = 5;

    canvas.width = scale * width;
    canvas.height = scale * height;
    var context = canvas.getContext('2d');


    context.beginPath();
    context.strokeStyle = "rgba(150, 255, 250, 1)";
    context.lineWidth = 1;
    for (let x = 0; x < width * scale; x += 100 * scale) {
        context.moveTo(x, height * scale);
        context.lineTo(x, 0);
    }
    for (let y = 0; y < height * scale; y += 100 * scale) {
        context.moveTo(width * scale, y);
        context.lineTo(0, y);
    }
    context.stroke();

    context.beginPath();
    context.strokeStyle = "rgba(255, 0, 0, 1)";
    context.lineWidth = 1;
    context.moveTo(0, 0);
    context.lineTo(0, height * scale);
    context.lineTo(width * scale, height * scale);
    context.lineTo(width * scale, 0);
    context.lineTo(0, 0);
    context.stroke();


    let draw_path = (on_g0, on_g1) => {
        let p = { x: 0, y: 0 };
        let origin = { x: 0, y: 0 };
        gcode.split("\n").filter(e => ((e.length > 0) && (e.match(/^G\d+/)))).forEach((v) => {
            const arr = v.match(/([GXYZF]\s*[\-]{0,1}[\s\d.\-]+\s*)/g);
            let kv = {};
            arr.forEach(e => { kv[e[0].toLowerCase()] = parseFloat(e.substring(1)); });
            if (kv.g !== undefined) {
                let np = { x: (kv.x != undefined) ? (kv.x * scale) : p.x, y: (kv.y != undefined) ? (kv.y * scale) : p.y };
                let pcanvas = { x: np.x - origin.x, y: height * scale - (np.y - origin.y) };
                if (kv.g == "0") {
                    //context.moveTo(pcanvas.x, pcanvas.y);
                    on_g0(pcanvas.x, pcanvas.y);
                    p = np;
                } else if (kv.g == "1") {
                    //context.lineTo(pcanvas.x, pcanvas.y);
                    on_g1(pcanvas.x, pcanvas.y);
                    p = np;
                } else if (kv.g == "92") {
                    p = np;
                    origin = np;
                }
            }
        });
    };

    let go_path = (style, line_width, drawing) => {
        context.beginPath();
        context.strokeStyle = style;
        context.moveTo(0, height * scale);
        context.lineWidth = line_width;
        context.lineCap = "round";
        context.lineJoin = "round";
        drawing();
        context.stroke();
    };
    if (bit_radius != undefined) {
        go_path("rgba(30, 30, 30, 0.1)", bit_radius * 2 * scale, () => {
            draw_path((x, y) => { context.moveTo(x, y); }, (x, y) => { context.lineTo(x, y); });
        });
    }

    go_path("rgba(30, 30, 30, 1)", 1, () => {
        draw_path((x, y) => { context.moveTo(x, y); }, (x, y) => { context.lineTo(x, y); });
    });

    go_path("rgba(130, 30, 30, 0.6)", 1, () => {
        draw_path((x, y) => { context.lineTo(x, y); }, (x, y) => { context.moveTo(x, y); });
    });

    context.font = '12px serif';
    let pnumber = 0;
    let new_shape = 1;
    draw_path((x, y) => { new_shape = 1; }, (x, y) => { 
        if (new_shape == 1) {pnumber++; context.fillText(pnumber, x, y);};new_shape = 0; });

};